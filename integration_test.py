from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


def _wait_for_page_load(driver, timeout):
    js = "return document.readyState === 'complete'"
    WebDriverWait(driver, timeout).until(lambda x: x.execute_script(js))


def _wait_for_url_change(driver, timeout, old_url):
    WebDriverWait(driver, timeout).until(EC.url_changes(old_url))


def test_native_login(selenium, service_url):
    timeout = 20
    selenium.get(service_url)
    login_url = selenium.current_url
    form = selenium.find_element_by_css_selector("form")
    login = form.find_element_by_name("user[login]")
    password = form.find_element_by_name("user[password]")
    login.send_keys("root")
    password.send_keys("nots3kR3t")
    form.submit()
    _wait_for_url_change(selenium, timeout, old_url=login_url)
    _wait_for_page_load(selenium, timeout)
    assert selenium.current_url == service_url
    assert selenium.title == "Projects · Dashboard · GitLab"


def test_oidc_login(selenium, service_url):
    timeout = 20
    selenium.get(service_url)
    login_url = selenium.current_url
    test_idp_button = selenium.find_element_by_id("oauth-login-openid_connect")
    test_idp_button.click()
    _wait_for_url_change(selenium, timeout, old_url=login_url)
    _wait_for_page_load(selenium, timeout)
    idp_url = selenium.current_url
    form = selenium.find_element_by_css_selector("form")
    login = form.find_element_by_name("login")
    password = form.find_element_by_name("password")
    login.send_keys("alice@example.com")
    password.send_keys("nots3kr3t")
    submit = form.find_element_by_css_selector("button")
    submit.click()
    _wait_for_url_change(selenium, timeout, old_url=idp_url)
    _wait_for_page_load(selenium, timeout)
    approval_url = selenium.current_url
    approve = selenium.find_element_by_css_selector("button.theme-btn--success")
    approve.click()
    _wait_for_url_change(selenium, timeout, old_url=approval_url)
    _wait_for_page_load(selenium, timeout)
    assert selenium.current_url == service_url
    assert selenium.title == "Projects · Dashboard · GitLab"
